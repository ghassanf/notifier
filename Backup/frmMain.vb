﻿Imports System.Data
Imports System.Data.Odbc
Imports System.IO
Imports System.Net.Mail


Public Class frmMain
#Region "Globals"
    Dim _Started As Boolean
    Public _format As New System.Globalization.CultureInfo("en-US", True)
#End Region

#Region "Events"
    Private Sub btnStartStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartStop.Click
        If _Started = False Then
            btnStartStop.Text = "Stop"
            _Started = True

            PBRefresh.Value = 0
            TimerRefresh.Start()
        Else
            btnStartStop.Text = "Start"
            _Started = False

            TimerRefresh.Stop()
        End If
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnStartStop.Text = "Stop"
        _Started = True

        PBRefresh.Value = 0
        TimerRefresh.Start()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        If _Started = True Then
            MsgBox("Please Stop the Synchronization!", MsgBoxStyle.Exclamation, "Sync!")
        Else
            Me.Close()
        End If
    End Sub

    Private Sub TimerRefresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerRefresh.Tick
        PBRefresh.PerformStep()

        If PBRefresh.Value = 100 Then
            PBRefresh.PerformStep()
            TimerRefresh.Stop()
            SendEmail()
            PBRefresh.Value = 0
            TimerRefresh.Start()
        End If
    End Sub

#End Region

#Region "Subs & Functions"
    Private Sub SendEmail()
        Dim connectionReadUnix, connectionUpdateUnix As New OdbcConnection
        Dim myReader As OdbcDataReader
        Dim cmdReadUnix, cmdUpdateUnix As New OdbcCommand
        Dim EmailNotificationRecord As New EmailNotification
        Dim emailClient As New SmtpClient("192.168.10.77")

        Dim SMTPUserInfo As New System.Net.NetworkCredential("ideal", "12345")
        emailClient.UseDefaultCredentials = False
        emailClient.Credentials = SMTPUserInfo

        'connectionReadUnix.ConnectionString = "Driver={Microsoft ODBC for Oracle};Server=UNIX.world;Uid=IDEAL;Pwd=IDEAL;"
        'connectionUpdateUnix.ConnectionString = "Driver={Microsoft ODBC for Oracle};Server=UNIX.world;Uid=IDEAL;Pwd=IDEAL;"
        connectionReadUnix.ConnectionString = "Driver={Microsoft ODBC for Oracle};Server=TEST.world;Uid=IDEAL;Pwd=IDEAL;"
        connectionUpdateUnix.ConnectionString = "Driver={Microsoft ODBC for Oracle};Server=TEST.world;Uid=IDEAL;Pwd=IDEAL;"

        Try
            connectionReadUnix.Open()
            connectionUpdateUnix.Open()

            cmdReadUnix.Connection = connectionReadUnix
            cmdUpdateUnix.Connection = connectionUpdateUnix
            cmdUpdateUnix.CommandType = CommandType.Text

            'Read the IH_EMR_EMAIL_NOTIFICATION

            cmdReadUnix.CommandText = "Select ID, SENDER, RECIPIENT, CC, SUBJECT, MAIL_BODY, STATUS, ATTACHMENT_PATH From IH_EMR_EMAIL_NOTIFICATION Where STATUS = 1"
            myReader = cmdReadUnix.ExecuteReader

            While myReader.Read
                If myReader.IsDBNull(0) = False Then
                    EmailNotificationRecord.ID = myReader.GetValue(0)
                Else
                    EmailNotificationRecord.ID = ""
                End If
                If myReader.IsDBNull(1) = False Then
                    EmailNotificationRecord.Sender = myReader.GetValue(1)
                Else
                    EmailNotificationRecord.Sender = ""
                End If
                If myReader.IsDBNull(2) = False Then
                    EmailNotificationRecord.Recipient = myReader.GetValue(2)
                Else
                    EmailNotificationRecord.Recipient = ""
                End If
                If myReader.IsDBNull(3) = False Then
                    EmailNotificationRecord.CC = myReader.GetValue(3)
                Else
                    EmailNotificationRecord.CC = ""
                End If
                If myReader.IsDBNull(4) = False Then
                    EmailNotificationRecord.Subject = myReader.GetValue(4)
                Else
                    EmailNotificationRecord.Subject = ""
                End If
                If myReader.IsDBNull(5) = False Then
                    EmailNotificationRecord.MailBody = myReader.GetValue(5)
                Else
                    EmailNotificationRecord.MailBody = ""
                End If
                If myReader.IsDBNull(6) = False Then
                    EmailNotificationRecord.Status = myReader.GetValue(6)
                Else
                    EmailNotificationRecord.Status = ""
                End If
                If myReader.IsDBNull(7) = False Then
                    EmailNotificationRecord.AttachmentPath = myReader.GetValue(7)
                Else
                    EmailNotificationRecord.AttachmentPath = ""
                End If
                Dim message As New MailMessage
                'message.CC.Clear()
                'message.Attachments.Clear()
                'message.To.Clear()

                Try
                    If InStr(EmailNotificationRecord.Sender, "@") Then
                        message.From = New MailAddress(LCase(EmailNotificationRecord.Sender))
                    Else
                        If UCase(EmailNotificationRecord.Sender) = "IDEAL APPLICATION" Or UCase(EmailNotificationRecord.Sender) = "" Then
                            message.From = New MailAddress("ideal@stgeorgehospital.org")
                        Else
                            message.From = New MailAddress(LCase(EmailNotificationRecord.Sender) + "@stgeorgehospital.org")
                        End If
                    End If

                    message.To.Add(EmailNotificationRecord.Recipient)

                    If EmailNotificationRecord.CC <> "" Then
                        message.CC.Add(EmailNotificationRecord.CC)
                    End If

                    message.Subject = EmailNotificationRecord.Subject
                    message.Body = EmailNotificationRecord.MailBody

                    If EmailNotificationRecord.AttachmentPath <> "" Then
                        message.Attachments.Add(New Attachment(EmailNotificationRecord.AttachmentPath))
                    End If

                    emailClient.Send(message)

                    cmdUpdateUnix.CommandText = "Update IH_EMR_EMAIL_NOTIFICATION Set STATUS = 2 Where ID = " + ReplaceNull(EmailNotificationRecord.ID)
                    cmdUpdateUnix.ExecuteNonQuery()
                Catch ex As Exception
                    Using w As StreamWriter = File.AppendText("c:\Program Files\Notifier\log.txt")
                        Log(ex.Message, w)
                        ' Close the writer and underlying file.
                        w.Close()
                        cmdUpdateUnix.CommandText = "Update IH_EMR_EMAIL_NOTIFICATION Set STATUS = 3 Where ID = " + ReplaceNull(EmailNotificationRecord.ID)
                        cmdUpdateUnix.ExecuteNonQuery()
                    End Using
                End Try
            End While

            myReader.Close()

            txt.Text = "Last synchronized on: " + DateAndTime.Now.ToString("dd/MM/yy HH:mm:ss")

            connectionReadUnix.Close()
            connectionUpdateUnix.Close()
        Catch ex As Exception
            Using w As StreamWriter = File.AppendText("c:\Program Files\Notifier\log.txt")
                Log(ex.Message, w)
                ' Close the writer and underlying file.
                w.Close()
            End Using
        End Try

    End Sub

    'Private Sub Synchronize()
    '    Dim connectionRead, connectionFrom, connectionTo As New OdbcConnection
    '    Dim myreader As OdbcDataReader
    '    Dim cmdRead, cmdFrom, cmdTo As New OdbcCommand
    '    Dim Record As New DataTransfer

    '    connectionRead.ConnectionString = "Driver={Microsoft ODBC for Oracle};Server=UNIX.world;Uid=IDEAL;Pwd=IDEAL;"
    '    connectionFrom.ConnectionString = "Driver={Microsoft ODBC for Oracle};Server=UNIX.world;Uid=IDEAL;Pwd=IDEAL;"
    '    connectionTo.ConnectionString = "Driver={Microsoft ODBC for Oracle};Server=LIS.world;Uid=IDEAL;Pwd=IDEAL;"

    '    connectionRead.Open()
    '    connectionFrom.Open()
    '    connectionTo.Open()

    '    Try
    '        cmdRead.Connection = connectionRead
    '        cmdFrom.Connection = connectionFrom
    '        cmdFrom.CommandType = CommandType.Text
    '        cmdTo.Connection = connectionTo
    '        cmdTo.CommandType = CommandType.Text

    '        cmdRead.CommandText = "Select C1,C2,C3,C4,C5,C6,DATE_CREATED From DATA_TRANSFER"
    '        myreader = cmdRead.ExecuteReader

    '        While myreader.Read
    '            If myreader.IsDBNull(0) = False Then
    '                Record.C1 = myreader.GetValue(0)
    '            Else
    '                Record.C1 = ""
    '            End If
    '            If myreader.IsDBNull(1) = False Then
    '                Record.C2 = myreader.GetValue(1)
    '            Else
    '                Record.C2 = ""
    '            End If
    '            If myreader.IsDBNull(2) = False Then
    '                Record.C3 = myreader.GetValue(2)
    '            Else
    '                Record.C3 = ""
    '            End If
    '            If myreader.IsDBNull(3) = False Then
    '                Record.C4 = myreader.GetValue(3)
    '            Else
    '                Record.C4 = ""
    '            End If
    '            If myreader.IsDBNull(4) = False Then
    '                Record.C5 = myreader.GetValue(4)
    '            Else
    '                Record.C5 = ""
    '            End If
    '            If myreader.IsDBNull(5) = False Then
    '                Record.C6 = myreader.GetValue(5)
    '            Else
    '                Record.C6 = ""
    '            End If
    '            If myreader.IsDBNull(6) = False Then
    '                Record.DateCreated = myreader.GetDate(6).ToString("dd/MM/yyyy HH:mm:ss")
    '            Else
    '                Record.DateCreated = ""
    '            End If

    '            cmdFrom.CommandText = "update DATA_TRANSFER set c5 = 1 where c1 = " + ReplaceNull(Record.C1)
    '            cmdFrom.ExecuteNonQuery()

    '            cmdTo.CommandText = "Insert Into DATA_TRANSFER(C1,C2,C3,C4,C5,C6,DATE_CREATED) VALUES (" + _
    '                                ReplaceNull(Record.C1) + "," + ReplaceNull(Record.C2) + ",'" + ReplaceEOS(Record.C3) + "','" + _
    '                                ReplaceEOS(Record.C4) + "'," + ReplaceNull(Record.C5) + "," + ReplaceNull(Record.C6) + ",to_date('" + Record.DateCreated + "','DD/MM/YYYY HH24:MI:SS'))"
    '            cmdTo.ExecuteNonQuery()

    '        End While

    '        myreader.Close()

    '        txt.Text = "Last synchronized on: " + DateAndTime.Now.ToString("dd/MM/yy HH:mm:ss")

    '        connectionRead.Close()
    '        connectionFrom.Close()
    '        connectionTo.Close()
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    'End Sub


    Public Function ReplaceNull(ByVal Number As String) As String
        If Number = "" Then
            Return "null"
        Else
            Return Number
        End If
    End Function

    Public Function ReplaceEOS(Optional ByVal str As String = "") As String
        Try
            If str.Length = 0 Then
                Return ""
            Else
                Return str.Replace("'", "''")
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Sub Log(ByVal logMessage As String, ByVal w As TextWriter)
        w.Write(ControlChars.CrLf)
        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
        w.WriteLine("{0}", logMessage)
        w.WriteLine("--------------------------------------------------------------------------")
        ' Update the underlying file.
        w.Flush()
    End Sub
#End Region

End Class
