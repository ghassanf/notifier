﻿Imports System.IO
Imports System.Net.Mail
Imports Oracle.DataAccess.Client

Public Class frmMain
#Region "Globals"
    Dim _Started As Boolean
    Public _format As New System.Globalization.CultureInfo("en-US", True)
#End Region

#Region "Events"
    Private Sub btnStartStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartStop.Click
        If _Started = False Then
            btnStartStop.Text = "Stop"
            _Started = True

            PBRefresh.Value = 0
            TimerRefresh.Start()
        Else
            btnStartStop.Text = "Start"
            _Started = False

            TimerRefresh.Stop()
        End If
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnStartStop.Text = "Stop"
        _Started = True

        PBRefresh.Value = 0
        TimerRefresh.Start()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        If _Started = True Then
            MsgBox("Please Stop the Synchronization!", MsgBoxStyle.Exclamation, "Sync!")
        Else
            Me.Close()
        End If
    End Sub

    Private Sub TimerRefresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerRefresh.Tick
        PBRefresh.PerformStep()

        If PBRefresh.Value = 100 Then
            PBRefresh.PerformStep()
            TimerRefresh.Stop()
            SendEmail()
            PBRefresh.Value = 0
            TimerRefresh.Start()
        End If
    End Sub

#End Region

#Region "Subs & Functions"
    Private Sub SendEmail()
        Dim oracleConnection As New OracleConnection("Data Source =(DESCRIPTION =(ADDRESS_LIST =(ADDRESS =(COMMUNITY = tcp.world)(PROTOCOL = TCP)(Host = 192.168.10.60) (Port = 1521))(ADDRESS =(COMMUNITY = tcp.world)(PROTOCOL = TCP)(Host = 192.168.10.60)(Port = 1526)))(CONNECT_DATA = (SID = ORCL)));User ID=ideal;Password=ideal;")
        Dim oracleCommand As New OracleCommand
        Dim myReader As OracleDataReader

        Dim EmailNotificationRecord As New EmailNotification
        Dim emailClient As New SmtpClient("webmailnew.stgeorgehospital.org")

        Dim isHtmlBody As Boolean = False

        Dim SMTPUserInfo As New Net.NetworkCredential("ideal", "12345")
        emailClient.UseDefaultCredentials = False
        emailClient.Credentials = SMTPUserInfo

        Try
            oracleConnection.Open()

            oracleCommand.Connection = oracleConnection
            oracleCommand.CommandText = "Select ID, SENDER, RECIPIENT, CC, SUBJECT, MAIL_BODY, STATUS, ATTACHMENT_PATH,IS_HTML_BODY,mail_body1 From IH_EMR_EMAIL_NOTIFICATION Where STATUS = 1"
            oracleCommand.CommandType = CommandType.Text
            myReader = oracleCommand.ExecuteReader()

            While (myReader.Read())
                If myReader.IsDBNull(0) = False Then
                    EmailNotificationRecord.ID = myReader.GetValue(0)
                Else
                    EmailNotificationRecord.ID = ""
                End If
                If myReader.IsDBNull(1) = False Then
                    EmailNotificationRecord.Sender = myReader.GetValue(1)
                Else
                    EmailNotificationRecord.Sender = ""
                End If
                If myReader.IsDBNull(2) = False Then
                    EmailNotificationRecord.Recipient = myReader.GetValue(2)
                Else
                    EmailNotificationRecord.Recipient = ""
                End If
                If myReader.IsDBNull(3) = False Then
                    EmailNotificationRecord.CC = myReader.GetValue(3)
                Else
                    EmailNotificationRecord.CC = ""
                End If
                If myReader.IsDBNull(4) = False Then
                    EmailNotificationRecord.Subject = myReader.GetValue(4)
                Else
                    EmailNotificationRecord.Subject = ""
                End If

                If myReader.IsDBNull(5) = False Then
                    EmailNotificationRecord.MailBody = myReader.GetValue(5)
                Else
                    EmailNotificationRecord.MailBody = ""
                End If

                If myReader.IsDBNull(6) = False Then
                    EmailNotificationRecord.Status = myReader.GetValue(6)
                Else
                    EmailNotificationRecord.Status = ""
                End If
                If myReader.IsDBNull(7) = False Then
                    EmailNotificationRecord.AttachmentPath = myReader.GetValue(7)
                Else
                    EmailNotificationRecord.AttachmentPath = ""
                End If

                If myReader.IsDBNull(8) = False Then
                    isHtmlBody = myReader.GetValue(8) - 1
                Else
                    isHtmlBody = False
                End If

                If myReader.IsDBNull(9) = False Then
                    EmailNotificationRecord.MailBody += myReader.GetValue(9)
                Else
                    EmailNotificationRecord.MailBody += ""
                End If

                Dim message As New MailMessage

                Try
                    'If InStr(EmailNotificationRecord.Sender, "@") Then
                    '    message.From = New MailAddress(LCase(EmailNotificationRecord.Sender))
                    'Else
                    '    If UCase(EmailNotificationRecord.Sender) = "IDEAL APPLICATION" Or UCase(EmailNotificationRecord.Sender) = "" Then
                    '        message.From = New MailAddress("ideal@stgeorgehospital.org")
                    '    Else
                    '        message.From = New MailAddress(LCase(EmailNotificationRecord.Sender) + "@stgeorgehospital.org")
                    '    End If
                    'End If

                    message.From = New MailAddress("ideal@stgeorgehospital.org")

                    message.To.Add(EmailNotificationRecord.Recipient)

                    If EmailNotificationRecord.CC <> "" Then
                        message.CC.Add(EmailNotificationRecord.CC)
                    End If

                    message.Subject = EmailNotificationRecord.Subject
                    message.Body = EmailNotificationRecord.MailBody

                    If EmailNotificationRecord.AttachmentPath <> "" Then
                        message.Attachments.Add(New Attachment(EmailNotificationRecord.AttachmentPath))
                    End If

                    message.IsBodyHtml = isHtmlBody
                    emailClient.Send(message)

                    oracleCommand.CommandText = "Update IH_EMR_EMAIL_NOTIFICATION Set STATUS = 2 Where ID = " + ReplaceNull(EmailNotificationRecord.ID)
                    oracleCommand.ExecuteNonQuery()
                Catch ex As Exception
                    Using w As StreamWriter = File.AppendText("c:\Program Files\Notifier\log.txt")
                        Log(ex.Message, w)
                        w.Close()
                        oracleCommand.CommandText = "Update IH_EMR_EMAIL_NOTIFICATION Set STATUS = 3 Where ID = " + ReplaceNull(EmailNotificationRecord.ID)
                        oracleCommand.ExecuteNonQuery()
                    End Using
                End Try
            End While

            myReader.Close()

            txt.Text = "Last synchronized on: " + DateAndTime.Now.ToString("dd/MM/yy HH:mm:ss")

            oracleConnection.Close()
        Catch ex As Exception
            Using w As StreamWriter = File.AppendText("c:\Program Files\Notifier\log.txt")
                Log(ex.Message, w)
                w.Close()
                oracleConnection.Close()
            End Using
        End Try

    End Sub

    Public Function ReplaceNull(ByVal Number As String) As String
        If Number = "" Then
            Return "null"
        Else
            Return Number
        End If
    End Function

    Public Function ReplaceEOS(Optional ByVal str As String = "") As String
        Try
            If str.Length = 0 Then
                Return ""
            Else
                Return str.Replace("'", "''")
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Sub Log(ByVal logMessage As String, ByVal w As TextWriter)
        w.Write(ControlChars.CrLf)
        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
        w.WriteLine("{0}", logMessage)
        w.WriteLine("--------------------------------------------------------------------------")
        ' Update the underlying file.
        w.Flush()
    End Sub
#End Region

End Class
